<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>True Trello Printer</title>
    <link href="css/style.css" rel="stylesheet">

    <STYLE type="text/css" media="print">
        body {
            margin: 0;
        }
    </STYLE>
</head>
<body>

<div class="main container">
    <div class="page">
        <div id="out">
            No Trello JSON data found
        </div>
        </ul>
    </div>
</div>
<script type="text/html" id="template-kayla2">

    {{#lists}}
    <div class="list {{name}} {{closed}}">
        <h1>{{name}}</h1>
        {{#cards}}
        <div class="card {{closed}}">
            <div class="card-body">
                <div class="row">
                    <h2>{{name}}</h2>

                    <div class="categories">
                        {{#labels.0}}
                        {{#labels}}
                        <div class="category {{color}}">
                        </div>
                        {{/labels}}
                        {{/labels.0}}
                    </div>
                    <div class="col-md-12 description">
                        {{{desc}}}
                    </div>
                    <div class="checklist">
                        {{#checklists.0}}
                        <div class="row checklists">
                            {{#checklists}}
                            <h4>{{name}}</h4>
                            <ul>
                                {{#checkItems}}
                                <li>{{name}}</li>
                                {{/checkItems}}
                            </ul>
                            {{/checklists}}
                        </div>
                        {{/checklists.0}}
                    </div>
                </div>

            </div>
        </div>
        {{/cards}}
    </div>
    {{/lists}}


</script>
<script type="text/html" id="template-kayla">
    {{#lists}}
    <div class="page">
        <h1>{{name}}</h1>
        {{#cards}}
        <div class="col-md-4 card {{labels.0.name}}">
            <div class="card-body">
                <div class="row">
                    <h2>{{{name}}}
                        {{#labels.0}}
                    <span class="pull-right categories">
                         <img src="imx/{{labels.0.name}}.png" alt="{{labels.0.name}}" class="{{labels.0.name}}"/>
                    </span>
                        {{/labels.0}}
                    </h2>
                </div>
                <div class="row">
                    <div class="description">{{{desc}}}</div>
                </div>
                <div class="row">
                    {{#attachments.0}}
                    <div class="col-sm-{{#checklists.0}}6{{/checklists.0}}{{^checklists.0}}12{{/checklists.0}} image">
                        <img src="{{url}}" alt="{{attachments.0.name}}">
                    </div>
                    {{/attachments.0}}
                    {{#checklists.0}}
                    {{#checklists}}
                    <div class="col-sm-6 checklists small">
                        <h5>{{name}}</h5>
                        <ul>
                            {{#checkItems}}
                            <li>{{name}}</li>
                            {{/checkItems}}
                        </ul>
                    </div>
                    {{/checklists}}
                    {{/checklists.0}}
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{#actions.0}}
                        <ul class="small list-group">

                            {{#actions}}
                            <li class="list-group-item">{{{text}}}</li>
                            {{/actions}}
                        </ul>
                        {{/actions.0}}
                    </div>
                </div>
            </div>
        </div>
        {{/cards}}
    </div>
    {{/lists}}

</script>

<script type="text/html" id="template-output">
    <div class="row">
        {{#lists}}
        <div class="col-sm-3 {{name}} list">
            <h1>{{name}}</h1>
            {{#cards}}
            <div class="col-sm-12 card {{closed}}">
                <div class="">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>{{name}}</h4>
                        </div>
                        <div class="col-sm-12">
                            {{{desc}}}
                            {{#actions.0}}
                            <ul class="list-group">
                                {{#actions}}
                                <li class="list-group-item"><tt style="color:gray;">{{date}} </tt> {{{text}}}</li>
                                {{/actions}}
                            </ul>
                            {{/actions.0}}
                        </div>
                    </div>
                    {{#attachments.0}}
                    <div class="col-sm-12 image">
                        <img src="{{url}}" alt="{{attachments.0.name}}">
                    </div>
                    {{/attachments.0}}
                    {{#checklists.0}}
                    <div class="col-sm-12 checklists">
                        {{#checklists}}
                        <h5>{{name}}</h5>
                        <ul>
                            {{#checkItems}}
                            <li>{{name}}</li>
                            {{/checkItems}}
                        </ul>
                        {{/checklists}}
                    </div>
                    {{/checklists.0}}
                </div>
            </div>
            {{/cards}}
        </div>
        {{/lists}}
    </div>
</script>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.7.2/mustache.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/marked/0.3.0/marked.min.js" type="text/javascript"></script>
<script type="text/javascript">

    function eatData(trelloJson) {
        var data = {
            board: trelloJson.name,
            lists: [],
            ref: {}
        }

        for (i in trelloJson.lists) {
            var list = trelloJson.lists[i]
            if (list.closed) {
                //continue
            }
            data.ref[list.id] = {
                name: list.name,
                cards: []
            }
            data.lists.push(data.ref[list.id])
        }


        var checklists = [];
        for (i in trelloJson.checklists) {
            checklists[trelloJson.checklists[i].id] = trelloJson.checklists[i];
        }

        for (i in trelloJson.cards) {
            var card = trelloJson.cards[i]
            if (!card.closed) {
                //continue

                data.ref[card.id] = {
                    name: card.name,
                    closed: card.closed ? "archived" : "",
                    desc: marked(card.desc),
                    actions: [],
                    attachments: card.attachments,
                    checklists: [],
                    labels: card.labels,
                    color: card.labels.color

                }

                for (i in card.idChecklists) {
                    data.ref[card.id].checklists[data.ref[card.id].checklists.length] = checklists[card.idChecklists[i]]
                }


                data.ref[card.idList].cards.push(data.ref[card.id])

            }
        }


        for (i in trelloJson.actions) {
            var action = trelloJson.actions[i]
            if (action.type != "commentCard") {
                continue
            }


            data.ref[action.id] = {
                text: marked(action.data.text),
                date: moment(action.date).format('YYYY-MM-DD')
            }
            try {
                data.ref[action.data.card.id].actions.push(data.ref[action.id])
            } catch (e) {
            }
        }
        for (var i in data.lists) { //every list
            var length = data.lists[i].cards
            var mod = data.lists[i].cards.length % 6;
            if (mod > 0) {
                var count = 6 - mod;
                for (var k = 0; k < count; k++) {
                    var next_index = data.lists[i].cards.length;
                    var name = "";
                    if (data.lists[i].cards[0].labels.length > 0) {
                        name = data.lists[i].cards[0].labels[0].name;
                    }

                    data.lists[i].cards[next_index] = {
                        "name": "&nbsp;",
                        "labels": [
                            {
                                "name": "empty " + name
                            }
                        ]
                    }
                }

            }
        }


        return data;
    }

    function showData(data) {
        var template = $('#template-kayla').html()
        console.log(JSON.stringify(data, null, 2))
        $('#out').html(Mustache.render(template, data))
    }

    function autorun() {
        if (null == data) {
            return alert('Please insert JSON data from Trello in the code')
        }
        showData(eatData(data));
    }
    if (document.addEventListener) document.addEventListener("DOMContentLoaded", autorun, false);
    else if (document.attachEvent) document.attachEvent("onreadystatechange", autorun);
    else window.onload = autorun;

    data = <?= file_get_contents("https://trello.com/b/WbmosgVx.json")?>;
</script>
</body>
</html>